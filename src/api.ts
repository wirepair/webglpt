const defaults = {
    "model": "gpt-3.5-turbo",
    "temperature": 0.7,
    "max_tokens": 256,
}

export default class ChatGPT {
    apiKey: string;

    // todo allow other params (model etc)
    constructor(apiKey : string) {
        this.apiKey = apiKey;
    }

    async ask(question : string) : Promise<string> {
        const params = {
            model: "gpt-3.5-turbo",
            messages: [{role: "user", content: question}],
            temperature: 0.7
          }
        const requestOptions = {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.apiKey,
          },
          body: JSON.stringify(params)
        };
        console.log('sent request %o', params);
        const response = await fetch('https://api.openai.com/v1/chat/completions', requestOptions);
        const data = await response.json();
        console.log('data: %o', data);
        return data.choices[0].message.content;
    }
}