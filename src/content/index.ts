import Overlay from "../components/Overlay.svelte";

// Some global styles on the page
//import "./styles.css";

document.addEventListener('selectionchange', function(e) {
    var selectedText = document.getSelection().toString();
    const msg = {'selection': selectedText};
    chrome.runtime.sendMessage(msg);
});

// Some JS on the page
// ...

// Some svelte component on the page, uncomment to show overlay in browser
//new Overlay({ target: document.body });
