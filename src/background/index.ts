chrome.runtime.onInstalled.addListener(() => {
    chrome.storage.session.get('apiKey').then(console.log);
});

chrome.runtime.onMessage.addListener((message, sender, sendResponse) => {
    if (message.selection) {
        chrome.storage.session.set({'selection': message.selection});
    }
});