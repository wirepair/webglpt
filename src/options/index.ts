import Options from "src/components/Options.svelte";

const target = document.getElementById("app");

function render() {
    chrome.storage.session.get('apiKey').then((v)  => {
        new Options({ target, props: {key: v.apiKey} });
    });
}
document.addEventListener("DOMContentLoaded", render);
