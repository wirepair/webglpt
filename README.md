# WebGLPT Chrome Extension Boiler Plate

A chrome extension that can call out to OpenAI. This is really just a starting point to allow you to add your own features! Play around!

---

<p align="center">
<img src="images/cap.png">
</p>

---

## Features:
- Demonstrates how to ask a question and include text from the page that is currently selected
- Examples interaction between page content <-> extension using messages
- Example overlay (disabled by default) [Uncomment Overlay to enable](src\content\index.ts)
- Built with Svelte, Typescript and Extension Manifest 3

### Notes (security):

- Stores API key in session storage, key will be removed when shut down. 
- Use `chrome.storage.local` if you wish to persist but be aware of the risks!


### TODOs

- Store chat history for more interactivity
- ???

## Development

```bash
# install dependencies
npm i

# build files to `/dist` directory
npm run dev
```

## Build

```bash
# build files to `/dist` directory
$ npm run build
```

## Setup

1. Create your [OpenAI key](https://platform.openai.com/account/api-keys)
2. Open the Extension Management page by navigating to `chrome://extensions`.
3. Enable Developer Mode by clicking the toggle switch next to `Developer mode`.
4. Click the `LOAD UNPACKED` button and select the `/dist` directory.

